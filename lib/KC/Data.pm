package KC::Data;

use 5.010001;
use strict;
use warnings;

use LWP::Simple qw($ua get);
use XML::Simple;
use DateTime;
use DBI;

require Exporter;

our @ISA = qw(Exporter);

our $DEBUG = $ENV{DEBUG} || 0;

# This allows declaration	use KC::Data ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = (
    'all' => [
        qw(
          get_dates ohloh_activity last5signoffs monthlyactivity yearlyactivity health_status get_devs
          set_now $now $cur_year $cur_month $cur_day $DEBUG
          )
    ]
);

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
);

our $VERSION = '0.01';

our ( $now, $cur_year, $cur_month, $cur_day );

sub set_now {
    my ( $dt ) = shift;
    $cur_year  = sprintf "%04d", $dt->year();
    $cur_month = sprintf "%02d", $dt->month();
    $cur_day   = sprintf "%02d", $dt->day();
}

sub last5signoffs {
    my $database = shift;
    my $sql =
"SELECT realname,bugs.bug_id,bug_when,short_desc                                            
    FROM bugs_activity,profiles,bugs WHERE bugs.product_id=2 AND bugs_activity.who=profiles.userid
      AND bugs.bug_id=bugs_activity.bug_id AND added='Signed Off' AND bugs.product_id = 2 
        ORDER BY bug_when DESC LIMIT 5";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute or die $sth->errstr;
    my $entries = $sth->fetchall_arrayref;
    return $entries;
}

sub monthlyactivity {
    my ( $database, $type ) = @_;
    my $sql =
"SELECT realname,count(*) FROM bugs_activity,profiles,bugs WHERE bugs_activity.who=profiles.userid AND bugs.bug_id=bugs_activity.bug_id AND added=? AND YEAR(bug_when) = ? AND MONTH(bug_when) = ? AND who != assigned_to GROUP BY realname,added ORDER BY count(*) desc;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($type, $cur_year, $cur_month) or die $sth->errstr;
    my $stats = $sth->fetchall_arrayref;
    return ($stats);
}

sub yearlyactivity {
    my ( $database, $type ) = @_;
    my $sql =
"SELECT realname,count(*) FROM bugs_activity,profiles,bugs WHERE bugs_activity.who=profiles.userid AND bugs.bug_id=bugs_activity.bug_id AND added=? AND YEAR(bug_when) = ? AND who != assigned_to GROUP BY realname,added ORDER BY count(*) desc;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($type, $cur_year) or die $sth->errstr;
    my $stats = $sth->fetchall_arrayref;
    return ($stats);
}

sub health_status {
    my ($database) = @_;
    my $nb_of_days = 60;
    my $sql        = q|
        SELECT bug_id, short_desc, bug_severity, bug_status, creation_ts, name
        FROM bugs
        LEFT JOIN components ON component_id=id
        WHERE bugs.product_id = 2
            AND bug_severity IN ( 'blocker', 'critical', 'major' )
            AND (
                    ( DATEDIFF(?, creation_ts) < ? AND bug_status IN ('NEW', 'Assigned', 'In Discussion', 'Failed QA', 'Patch doesn\'t apply') )
                OR  ( bug_status IN ('Needs Signoff', 'Signed Off', 'Passed QA') )
            )
        ORDER BY FIELD(bug_severity, 'blocker', 'critical', 'major'), FIELD(bug_status, 'NEW', 'Assigned', 'In Discussion', 'Failed QA', 'Patch doesn\'t apply', 'Needs Signoff', 'Signed Off', 'Passed QA');
    |;
    my ( @bugs, @current_severity_bunch, @current_status_bunch, $last_severity,
        $last_status, $total );
    for my $bug (
        @{ $database->selectall_arrayref( $sql, { Slice => {} }, sprintf("%s-%s-%s", $cur_year, $cur_month, $cur_day), $nb_of_days ) }
      )
    {
        $total->{ $bug->{bug_severity} }++;
        unless (@current_status_bunch) {
            push @current_status_bunch, $bug;
        }
        else {
            if (   $last_status ne $bug->{bug_status}
                or $last_severity ne $bug->{bug_severity} )
            {
                push @current_severity_bunch,
                  { status => $last_status, bugs => [@current_status_bunch] };
                @current_status_bunch = ();
            }
            if ( $last_severity ne $bug->{bug_severity} ) {
                push @bugs,
                  {
                    severity => $last_severity,
                    statuses => [@current_severity_bunch]
                  };
                @current_severity_bunch = ();
            }
            push @current_status_bunch, $bug;
        }
        $last_severity = $bug->{bug_severity};
        $last_status   = $bug->{bug_status};
    }
    push @current_severity_bunch,
      { status => $last_status, bugs => [@current_status_bunch] };
    push @bugs,
      { severity => $last_severity, statuses => [@current_severity_bunch] };

    return { bugs => \@bugs, total => $total };
}

sub get_dates {

    # subroutine to parse a date file and put it into a form suitable for TT
    open( my $FH, '<', 'data/dates.txt' );
    my @dates;
    my $today = DateTime->now->ymd;
    while ( my $line = <$FH> ) {
        my ( $date, $desc ) = split( /\|/, $line );
        my $daterow = {
            'date' => $date,
            'desc' => $desc
        };
        if ( $date gt $today ) {
            push @dates, $daterow;
        }

    }
    close $FH;
    return \@dates;

}

sub ohloh_activity {
    my $url =
"http://www.ohloh.net/projects/koha/analyses/latest/activity_facts.xml?api_key=ad98f4080e21c596b62c9315f6c7a4c8b08af082";

    # get the url from the server
    my $response = get $url or return;

    # parse the XML response
    my $xml = eval { XMLin($response) } or return;

    # was the request a success?
    return
      unless $xml->{status} eq 'success';
    return $xml;
}

sub get_devs {
    my $api_key = shift;
    my $url = "https://www.openhub.net/projects/koha/contributors.xml?sort=newest&api_key=$api_key";

    # get the url from the server
    my $response = get $url or return;

    # parse the XML response
    my $xml = eval { XMLin($response) } or return;
    # was the request a success?
    return
      unless $xml->{status} eq 'success';
    return $xml;
}

1;

__END__
